import requests
from django_cache import settings


class BaseRequest:

    def __init__(self):
        self._session = requests.Session()
        self.timeout = settings.REQUEST_TIMEOUT

    def _get_action(self, url: str, data=None, timeout=None, **kwargs):
        try:
            with self._session as session:
                self.timeout = self.timeout if timeout is None else timeout
                response = session.get(url, data=data, timeout=self.timeout, **kwargs)
                return response, response.status_code
        except (requests.ConnectionError, requests.Timeout) as err:
            return None, 500

    def _post_action(self, url: str, data, timeout=None, **kwargs):
        try:
            with self._session as session:
                self.timeout = self.timeout if timeout is None else timeout
                response = session.post(url, json=data, timeout=self.timeout, **kwargs)
                return response, response.status_code
        except (requests.ConnectionError, requests.Timeout) as err:
            return None, 500

    def _put_action(self, url: str, data: dict, timeout=None, **kwargs):
        try:
            with self._session as session:
                self.timeout = self.timeout if timeout is None else timeout
                response = session.put(url, json=data, timeout=self.timeout, **kwargs)
                return response, response.status_code
        except (requests.ConnectionError, requests.Timeout) as err:
            return None, 500

    def _delete_action(self, url: str, data=None, timeout=None, **kwargs):
        try:
            with self._session as session:
                self.timeout = self.timeout if timeout is None else timeout
                response = session.delete(url, data=data, timeout=self.timeout, **kwargs)
                return response, response.status_code
        except (requests.ConnectionError, requests.Timeout) as err:
            return None, 500
