from django.utils import timezone
from django.forms import model_to_dict
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from auth_management.utils import generate_refresh_token

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    refresh = serializers.CharField(max_length=255, read_only=True)
    password_reset = serializers.BooleanField(default=False)

    def validate(self, data):
        username = data.get("username", None)
        password = data.get("password", None)
        user = authenticate(username=username, password=password)

        if user is None:
            attempt_data = {
                'username': username,
            }

            raise serializers.ValidationError(
                'A user with this username and password is not found.'
            )

        try:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            jwt_refresh = generate_refresh_token(user)
            update_last_login(None, user)
        except user.DoesNotExist:
            raise serializers.ValidationError(
                'User with given username and password does not exists'
            )

        res_data = {
            'username': user.username,
            'token': jwt_token,
            'refresh': jwt_refresh,
            'jwt_refresh': jwt_refresh
        }
        return res_data


class UserSetPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True, max_length=20)
    password = serializers.CharField(required=True, max_length=20)
    confirm_password = serializers.CharField(required=True, max_length=20)


class RefreshTokenSerializer(serializers.Serializer):
    refresh = serializers.CharField(max_length=255, required=True)
