import jwt
import datetime
from django.conf import settings
from rest_framework_jwt.settings import api_settings
from django.contrib.auth.models import update_last_login
from rest_framework import exceptions

from django.contrib.auth.models import User

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


def generate_token(user):
    try:
        payload = JWT_PAYLOAD_HANDLER(user)
        jwt_token = JWT_ENCODE_HANDLER(payload)
        update_last_login(None, user)
        res_data = {
            'token': jwt_token,
            'refresh': generate_refresh_token(user),
        }
        return res_data
    except Exception as E:
        print(E)
        return False


def generate_refresh_token(user):
    refresh_token_payload = {
        'username': user.username,
        'exp': datetime.datetime.utcnow() + settings.JWT_AUTH['JWT_REFRESH_EXPIRATION_DELTA']
    }
    refresh_token = jwt.encode(
        refresh_token_payload, settings.JWT_AUTH['JWT_SECRET_KEY'],
        algorithm=settings.JWT_AUTH['JWT_ALGORITHM']).decode('utf-8')
    return refresh_token


def check_refresh_token(refresh_token):
    if refresh_token is None:
        raise exceptions.AuthenticationFailed(
            'Authentication credentials were not provided.')
    try:
        payload = jwt.decode(
            refresh_token, settings.JWT_AUTH['JWT_SECRET_KEY'], algorithms=settings.JWT_AUTH['JWT_ALGORITHM'])
    except jwt.ExpiredSignatureError:
        raise exceptions.AuthenticationFailed(
            'expired refresh token, please login again.')

    user = User.objects.filter(username=payload.get('username')).first()
    if user is None:
        raise exceptions.AuthenticationFailed('User not found')
    token_info = generate_token(user)
    return token_info


def get_client_ip(request):
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
    except Exception as E:
        print(E)
        return None
