from django.urls import path
from auth_management.views import UserLoginView, UserSetPasswordView, \
    UserRefreshView

urlpatterns = [
    path('web/v1/login/', UserLoginView.as_view()),
    path('web/v1/refresh/', UserRefreshView.as_view()),
    path('web/v1/set_password/', UserSetPasswordView.as_view()),
]
