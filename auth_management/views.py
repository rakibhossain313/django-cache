from django.contrib.auth.hashers import make_password
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import User
from auth_management.serializers import UserLoginSerializer, UserSetPasswordSerializer, RefreshTokenSerializer
from auth_management.utils import check_refresh_token
from django_cache_response.services.auth_management import AuthManagementMessage as ARM
from django_cache_response.services.auth_management import AuthManagementCode as ARC


class UserLoginView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            res_data = {
                'code': ARC.JWT_TOKEN_SUCCESS_CREATED_CODE,
                'message': ARM.JWT_TOKEN_SUCCESS_CREATED_MSG,
                'token': serializer.data['token'],
                'refresh': serializer.data['refresh'],
                'password_reset': serializer.data['password_reset'],
            }
            return Response(res_data, status=status.HTTP_200_OK)
        return Response({
            'code': ARC.JWT_TOKEN_ERROR_CODE,
            'message': ARM.JWT_TOKEN_ERROR_MSG,
            'errors': serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)


class UserSetPasswordView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication
    serializer_class = UserSetPasswordSerializer

    def post(self, request):
        req_user = request.user
        req_data = request.data
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            if not req_data['password'] == req_data['confirm_password']:
                return Response({
                    'code': ARC.PASSWORD_CONFIRM_PASSWORD_NOT_MATCHED_CODE,
                    'message': ARM.PASSWORD_CONFIRM_PASSWORD_NOT_MATCHED_MSG,
                }, status=status.HTTP_404_NOT_FOUND)

            user = User.objects.get(username=req_user)
            if not user.check_password(req_data['old_password']):
                return Response({
                    'code': ARC.CURRENT_PASSWORD_NOT_MATCHED_CODE,
                    'message': ARM.CURRENT_PASSWORD_NOT_MATCHED_MSG,
                }, status=status.HTTP_404_NOT_FOUND)

            user.password = make_password(req_data['password'])
            user.password_reset = True
            user.save()
            return Response({
                'code': ARC.SET_PASSWORD_SUCCESS_CODE,
                'message': ARM.SET_PASSWORD_SUCCESS_MSG
            }, status=status.HTTP_200_OK)


class UserRefreshView(APIView):
    permission_classes = (AllowAny,)
    authentication_class = JSONWebTokenAuthentication
    serializer_class = RefreshTokenSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            print(serializer.data)
            refresh_token_data = check_refresh_token(serializer.data['refresh'])
            if refresh_token_data:
                res_data = {
                    'code': ARC.JWT_TOKEN_SUCCESS_CREATED_CODE,
                    'message': ARM.JWT_TOKEN_SUCCESS_CREATED_MSG,
                    'token': refresh_token_data['token'],
                    'refresh': refresh_token_data['refresh'],
                    'permission_data': refresh_token_data['permission_list']
                }
                return Response(res_data, status=status.HTTP_200_OK)
        return Response({
            'code': ARC.JWT_TOKEN_ERROR_CODE,
            'message': ARM.JWT_TOKEN_ERROR_MSG,
        }, status=status.HTTP_400_BAD_REQUEST)
