from rest_framework import serializers


class ProductModelSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=None, allow_blank=True)
    price = serializers.IntegerField(max_value=None, min_value=1)
