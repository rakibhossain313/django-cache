from django.urls import path

from store.views import ProductListView, ProductCreateView

urlpatterns = [
    # product list
    path('web/v1/product/create/', ProductCreateView.as_view()),
    path('web/v1/product/list/', ProductListView.as_view()),
]
