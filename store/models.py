import logging
import uuid
from django.db import models

logger = logging.getLogger('general')


# Create your models here.
class ProductManager(models.Manager):

    def create_product(self, req_data):
        return self.create(
            name=req_data['name'],
            price=req_data['price'],
            description=req_data['description']
        )

    def get_all_product(self):
        try:
            return self.filter(is_active=True).values()
        except Exception as e:
            logger.debug({'payload': '', 'response': 'Product not found', 'error': e})
            return False

    def is_exist(self, data):
        try:
            return self.filter(is_active=True, name=data['name'])
        except Exception as e:
            logger.debug({'payload': '', 'response': 'Product not found', 'error': e})
            return False


class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    objects = ProductManager()

    def __str__(self):
        return str(self.name)
