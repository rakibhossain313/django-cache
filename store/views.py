import logging
import random
from datetime import datetime
from itertools import chain

from django.db import transaction
from django.forms import model_to_dict
from django.utils.decorators import method_decorator
from django.conf import settings
from django.core.cache import cache

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from applibs.decorator.message_lang import lang
from applibs.pagination import LimitOffsetOrAllPagination
from django_cache_response.services.store import StoreMessage as SM
from django_cache_response.services.store import StoreResponseCode as SRC
from store.models import Product
from store.serializers import ProductModelSerializer

logger = logging.getLogger("general")


class ProductCreateView(APIView):

    def post(self, request):
        serializer = ProductModelSerializer(data=request.data)
        if serializer.is_valid():
            create_data = serializer.data
            create_data['req_user'] = request.user

            is_exist = Product.objects.is_exist(create_data)
            if is_exist:
                logger.debug({"response": SM.PRODUCT_ALREADY_EXIST})
                return Response({
                    'code': SRC.PRODUCT_ALREADY_EXIST,
                    'message': SM.PRODUCT_ALREADY_EXIST,
                    'data': {}
                }, status=status.HTTP_409_CONFLICT)

            product = Product.objects.create_product(create_data)
            logger.debug({"response": model_to_dict(product)})
            # clear cache data
            cache.clear()
            return Response({
                'code': SRC.PRODUCT_CREATED,
                'message': SM.PRODUCT_CREATED,
                'data': model_to_dict(product)
            }, status=status.HTTP_201_CREATED)

        logger.debug({"serializer errors": serializer.errors})
        return Response({
            'code': SRC.DATA_VALIDATION_ERROR,
            'message': SM.DATA_VALIDATION_ERROR,
            'error': serializer.errors,
            'data': {},
        }, status=status.HTTP_400_BAD_REQUEST)


class ProductListView(APIView, LimitOffsetOrAllPagination):

    @method_decorator(lang)
    def get(self, request, **kwargs):
        if 'product' in cache:
            # get results from cache
            product_list = cache.get('product')
            if product_list:
                product_list_subset = self.paginate_queryset(product_list, request, view=self)
                product_list_paginated = self.get_paginated_response(product_list_subset)

                logger.debug({"response": product_list_paginated.data})

                return Response({
                    'data': product_list_paginated.data,
                    'message': SM.PRODUCT_LIST_FOUND[kwargs['lang']],
                    'code': SRC.PRODUCT_LIST_FOUND,
                    "lang": kwargs['lang']
                })
        else:
            product_list = Product.objects.get_all_product()
            # store data in cache
            cache.set('product', product_list, timeout=settings.CACHE_TTL)
            if product_list:
                product_list_subset = self.paginate_queryset(product_list, request, view=self)
                product_list_paginated = self.get_paginated_response(product_list_subset)

                logger.debug({"response": product_list_paginated.data})

                return Response({
                    'data': product_list_paginated.data,
                    'message': SM.PRODUCT_LIST_FOUND[kwargs['lang']],
                    'code': SRC.PRODUCT_LIST_FOUND,
                    "lang": kwargs['lang']
                })
            return Response({
                'data': '',
                'message': SM.PRODUCT_LIST_NOT_FOUND[kwargs['lang']],
                'code': SRC.PRODUCT_LIST_NOT_FOUND,
                "lang": kwargs['lang']
            })
