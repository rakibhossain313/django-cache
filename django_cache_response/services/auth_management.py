from django_cache_response import Message, ResponseCode


# Write your message here
class AuthManagementMessage(Message):
    JWT_TOKEN_SUCCESS_CREATED_MSG = 'Login success!.'
    CURRENT_PASSWORD_NOT_MATCHED_MSG = 'Current password not matched.'
    PASSWORD_CONFIRM_PASSWORD_NOT_MATCHED_MSG = 'Password and confirm password not matched.'
    SET_PASSWORD_SUCCESS_MSG = 'Successfully password set.'
    JWT_TOKEN_ERROR_MSG = 'Something went wrong please provide valid credentials.'


# Write your response code here
class AuthManagementCode(ResponseCode):
    JWT_TOKEN_SUCCESS_CREATED_CODE = 'AMJ_RC200'
    CURRENT_PASSWORD_NOT_MATCHED_CODE = 'AMP_NM404'
    PASSWORD_CONFIRM_PASSWORD_NOT_MATCHED_CODE = 'AMP_CPNM404'
    SET_PASSWORD_SUCCESS_CODE = 'AMP_SP200'
    JWT_TOKEN_ERROR_CODE = 'AMJ_TE400'

