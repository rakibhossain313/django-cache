# Write your message here
from django_cache_response import Message, ResponseCode


class StoreMessage(Message):
    DATA_VALIDATION_ERROR = {'en': 'Please provide valid input.', 'bn': ''}

    # Route starts here
    PRODUCT_ALREADY_EXIST = {'en': 'Product already exists.', 'bn': 'পণ্য ডাটাবেস এ আগে থেকেই আছে'}
    PRODUCT_CREATED = {'en': 'Product Created Successfully.', 'bn': 'পণ্য সফলভাবে তৈরি করা হয়েছে'}
    PRODUCT_UPDATED = {'en': 'Product Updated Successfully.', 'bn': 'পণ্য সফলভাবে আপডেট করা হয়েছে'}
    PRODUCT_NOT_FOUND = {'en': 'Product not found.', 'bn': 'পণ্য পাওয়া যায় নি'}
    PRODUCT_LIST_FOUND = {'en': 'Product list found.', 'bn': 'পণ্য পাওয়া যায় গেছে'}
    PRODUCT_LIST_NOT_FOUND = {'en': 'Product list not found.', 'bn': 'পণ্য পাওয়া যায়নি'}
    PRODUCT_DETAIL_FOUND = {'en': 'Product detail found.', 'bn': 'পণ্য পাওয়া যায় গেছে'}


# Write your response code here
class StoreResponseCode(ResponseCode):
    DATA_VALIDATION_ERROR = 'DVE4001'

    # Route starts here
    PRODUCT_CREATED = 'PC2011'
    PRODUCT_ALREADY_EXIST = 'PAE4002'
    PRODUCT_NOT_FOUND = 'PNF4043'
    PRODUCT_UPDATED = 'PU2012'
    PRODUCT_LIST_FOUND = 'PLF2001'
    PRODUCT_LIST_NOT_FOUND = 'PLNF4004'
    PRODUCT_DETAIL_FOUND = 'PDF2002'
