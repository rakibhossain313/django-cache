class Message:
    USER_PERMISSION_NOT_FOUND = {'en': 'You are not authorized to perform this request.', 'bn': ''}
    USER_TOKEN_INVALID = {'en': 'You are not authorized to perform this request.', 'bn': ''}
    USER_TOKEN_NOT_FOUND = {'en': 'Authentication credentials were not provided.', 'bn': ''}
    INVALID_CREDENTIALS = {'en': 'Password is incorrect. Please try again.', 'bn': ''}
    INVALID_CREDENTIALS_ATTENTION = {'en': 'Attention! One more incorrect attempt will lock your account.', 'bn': ''}
    ACCOUNT_LOCK = {'en': 'Locked! Your account has been locked! Please try again after {}.', 'bn': ''}
    PROVIDE_VALID_DATA = {'en': 'Please provide valid data.', 'bn': ''}
    ACCOUNT_NOT_ACTIVE = {'en': 'Your account is not active yet. Please try later.', 'bn': ''}


class ResponseCode:
    # for 200 status code
    DATA_FOUND = 'DF200'

    # for 201 status code
    SERVICE_CREATED = 'SC201'

    # for 304 status code
    DATA_NOT_MODIFIED = 'DNM304'

    # for 400 status code
    INVALID_REQUEST = 'IR400'

    # for 401 status code
    UNAUTHORIZED_USER = 'UU401'

    # for 403 status code
    REQUEST_FORBIDDEN = 'RF403'

    # for 404 status code
    DATA_NOT_FOUND = 'DNF404'

    # for 405 status code
    METHOD_NOT_ALLOWED = 'MNA405'

    # for 500 status code
    INTERNAL_SERVER_ERROR = 'ISE500'

    # for 503 status code
    SERVICE_UNAVAILABLE = 'SU503'

    # Wallet locked for wrong pin
    WALLET_LOCKED = 'WALLET_LOCKED_423'

